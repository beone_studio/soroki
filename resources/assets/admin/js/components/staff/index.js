import Staff from './Staff.vue'

export default [{
    path: '/staff',
    component: Staff,
    props: {
        title: 'Персонал',
        icon: 'list',
    }
}]