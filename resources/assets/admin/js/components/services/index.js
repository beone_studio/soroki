import Table from './table.vue'
import Layout from './layout.vue'
import Create from './create.vue'
import Edit from './edit.vue'

export default [{
    path: '/services',
    component: Layout,
    props: {
        title: 'Услуги',
        icon: 'list',
    },
    children: [
    {
		path: '', 
        name: 'Список услуг',
        component: Table, 
        props: {
            title: 'Список услуг',
            showInMenu: true
        },    	
    },
	{
		path: 'create', 
        name: 'Создание услуги',
        component: Create, 
        props: {
            title: 'Создание услуги',
            showInMenu: true
        },    	
    }, 
	{
		path: ':id', 
        name: 'Редактирование услуги',
        component: Edit, 
        props: {
            title: 'Редактирование услуги',
            showInMenu: false
        },    	
    },       
    ]
}]