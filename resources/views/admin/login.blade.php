<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Администраторская</title>
    <link rel="stylesheet" href="{{ mix('/assets/admin/css/app.css') }}">
    <link rel="stylesheet" href="{{ mix('/assets/admin/css/style.css') }}">
</head>
<body>
<!--
<div class="login-page">
    <div class="panel login-form">
        <div class="panel-body">
            <form method="post">
                <h1>Панель управления</h1>
                {{ csrf_field() }}
                <input type="text" placeholder="Логин" name="username"/>
                <input type="password" placeholder="Пароль" name="password"/>
                <button>Вход</button>
                @if ($error)
                    <div class="message">Неверный логин/пароль</div>
                @endif
            </form>
        </div>
    </div>
</div>
-->
<div class="wrapper-page">
            <div class="panel panel-color panel-primary panel-pages">
                <div class="panel-heading bg-img"> 
                    <div class="bg-overlay"></div>
                    <h3 class="text-center m-t-10 text-white"> Администраторская</h3>
                </div> 


                <div class="panel-body">
                <form class="form-horizontal m-t-20" method="post">
                    {{ csrf_field() }}
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control input-lg " name="username" type="text" required="" placeholder="Пользователь">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control input-lg" name="password" type="password" required="" placeholder="Пароль">
                        </div>
                    </div>

                    <!--<div class="form-group ">
                        <div class="col-xs-12">
                            <div class="checkbox checkbox-primary">
                                <input id="checkbox-signup" type="checkbox">
                                <label for="checkbox-signup">
                                    Remember me
                                </label>
                            </div>
                            
                        </div>
                    </div>-->
                    
                    <div class="form-group text-center m-t-40">
                        <div class="col-xs-12">
                            <button class="btn btn-primary btn-lg w-lg waves-effect waves-light" type="submit">Войти</button>
                        </div>
                    </div>

                    @if ($error)
                        <div class="form-group text-center m-t-40">
                            <div class="error">Неверный логин или пароль</div>
                        </div>
                    @endif
                    
                    <!--<div class="form-group m-t-30">
                        <div class="col-sm-7">
                            <a href="recoverpw.html"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a>
                        </div>
                        <div class="col-sm-5 text-right">
                            <a href="register.html">Create an account</a>
                        </div>
                    </div>
                    -->
                </form> 
                </div>                                 
                
            </div>
        </div>
</body>
</html>