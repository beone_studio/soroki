<!DOCTYPE html>
<html>
	<head>
		<title>Сороки</title>
		<link rel="stylesheet" href="/assets/index/css/about.css">
		<link rel="stylesheet" href="/assets/index/css/media_about.css">
		<link rel="stylesheet" href="/assets/index/css/mobileMenu.css">
		<link rel="stylesheet" href="/assets/index/css/font-awesome.min.css">
		<link rel="shortcut icon" href="/assets/index/img/ico.png">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">		
	</head>
	<body>
		<div class="mobileBlock">
			<div class="mobileMenu">
				<a href="" id="closeMenu" class="closeMenu"><span class="fa fa-times"></span></a>
				<ul class="menu">
					<li><a href="/">Главная</a></li>
					<li><a href="/about">О нас</a></li>
					<!-- <li><a href="/services">Услуги</a></li> -->
					<li><a href="/contacts">Контакты</a></li>
				</ul>				
			</div>
			<div class="header">
				<a id="openMenu" class="fa fa-bars"></a>
				<a class="tel" href="tel:+375296992930">+375 29 699-29-30</a>
			</div>
			<div class="content">
				<h2 class="title">О нас</h2>
				<p class="description">
					"Сороки" - это ассоциативный образ встречи подруг, которые умеют совмещать приятное с полезным: уход за собой и веселый светский разговор. К тому же сорока - умная птица и "очень женщина" - любит все яркое, блестящее, видит и узнает себя в зеркале, а в восточной культуре эта птица является символом счастья и приносит радость.
				</p>
				<p class="description">
					Уникальность нашего салона начинается с его местоположения - старейшее здание в историческом квартале в центре Минска - и заканчивается личностью каждого сотрудника. Мы постарались сохранить экологичность "экологичность" исторического места, то есть соединили экостиль с современными дизайнерскими решениями, и это вполне отвечает миксовому дизайну 21 века, в котором отлично сочетаются старинная архитектура и современность..
				</p>
				<p class="description">
					Главная цель салона - создать атмосферу и обслуживание класса люкс для постоянно бегущих и быстроживущих современных минчан.
				</p>
				<a href="" id="nextSlide" class="nextSlide"><span class="fa fa-arrow-right"></span></a>
			</div>
			<div class="mobileSiema">
				<div class="slide">
					<div class="photo">
						<img src="/assets/index/img/main.jpg" alt="">
					</div>							
				</div>
				<div class="slide">
					<div class="photo">
						<img src="/assets/index/img/main2.jpg" alt="">
					</div>							
				</div>								
			</div>
			<a class="nodecoration" href="/contacts">
				<div class="goToServices">
					<div>
						<span class="fa fa-arrow-down"></span>
						<p>Контакты</p>
					</div>
				</div>
			</a>
		</div>
		<div class="aboutBlock" id="viewport1">
			<div class="defaultClip">
				<a href="/" class="goMain"><span class="fa fa-arrow-left"></span><br><br>Вернуться <br>на главную</a>
				<div class="leftBlock">
					<ul class="menu" id="menu1">
						<li><a href="" id="menu1Viewport1">Довольные клиенты</a></li>
					</ul>
				</div>				
			</div>
			<div class="container">
				<div class="content slider">
					<div class="siema">					
						<div class="slide">
							<div class="text">
								<h2 class="slideTitle">Комплимент от<br>Кристины</h2>
								<i class="increasedSymbol">"</i>
								<p class="description">Клиентинг, клиентинг и еще раз – клиентинг.
								Лучшее и правдивейшее исполнение девиза в городе.</p>
								<p class="employeeName">17.12.2017</p>
							</div>
							<div class="photo">
								<img src="/assets/index/img/k1.jpg" alt="">
							</div>							
						</div>
						<div class="slide">
							<div class="text">
								<h2 class="slideTitle">Комплимент от<br>Вероники</h2>
								<i class="increasedSymbol">"</i>
								<p class="description">Долговременное покрытие! Продержалось месяц!</p>
								<p class="employeeName">22.12.2017</p>
							</div>
							<div class="photo">
								<img src="/assets/index/img/k1.jpg" alt="">
							</div>							
						</div>								
						<div class="slide">
							<div class="text">
								<h2 class="slideTitle">Комплимент от<br>Светланы</h2>
								<i class="increasedSymbol">"</i>
								<p class="description">Огромное спасибо милым девушкам, мастеру Анастасии и администратору! Очень уютный салон! С наилучшими пожеланиями!</p>
								<p class="employeeName">15.01.2018</p>
							</div>
							<div class="photo">
								<img src="/assets/index/img/k1.jpg" alt="">
							</div>							
						</div>	
						<div class="slide">
							<div class="text">
								<h2 class="slideTitle">Комплимент от<br>Виктории</h2>
								<i class="increasedSymbol">"</i>
								<p class="description">Сеть из двух салонов с огромным опытом и массовой рекламой в СМИ привлекает самых разных клиентов – это заметно, когда заходишь в любой из салонов. Мне готовы были выполнить дизайн любой сложности: от зеркальной втирки до рисунков от руки. Девушки милые. Цены адекватные </p>
								<p class="employeeName">17.01.2018</p>
							</div>
							<div class="photo">
								<img src="/assets/index/img/k1.jpg" alt="">
							</div>							
						</div>												
					</div>
					<div class="buttons">
						<button id="prevSiema"><span class="fa fa-arrow-left"></span></button>
						<button id="nextSiema"><span class="fa fa-arrow-right"></span></button>
					</div>					
				</div>
			</div>	
			<div class="footer">
				<div class="soc_networks">
					<a target="_blank" href="https://www.instagram.com/salon_soroki/" class="fa fa-instagram"></a>
					<a target="_blank" href="https://www.facebook.com/salonsoroki/" class="fa fa-facebook"></a>
				</div>
				<p>г.Минск, ул.Революционная 7<br>+375 (29) 699-29-30</p>
				<p>&copy 2008-2017 ООО Дефиле<br>Все права защищены</p>
			</div>									
		</div>							
	</body>
	<script src="/assets/index/js/siema.min.js"></script>
	<script src="/assets/index/js/jquery-3.2.1.min.js"></script>
	<script>
		window.onload = function () {
			$('html,body').css('opacity', '1');
			for(let j = 1; j <= 2; j++) {
				for(let i = 1; i <= $('#menu' + j).children().length; i++) {
					$('#menu' + j + 'Viewport' + i).on('click', (event) => {
						event.preventDefault();
						let destination = $('#viewport' + i).position().top;
						$('HTML, BODY').animate({ scrollTop: destination }, 1000);		
					});
				}
			}	
			$('#openMenu').on('click', function (event) {
				event.preventDefault();
				$(".mobileMenu").css('left') == '0px' ? $(".mobileMenu").css('left','-100%') : $(".mobileMenu").css('left','0');
			});	
			$('#closeMenu').on('click', function (event) {
				event.preventDefault();
				$(".mobileMenu").css('left', '-100%');
			})	
		};
	 	var curCollage = {
	 		largePhotoPosition: {
	 			prev: {marginLeft: '-60px'},
	 			cur: {marginLeft: '0px'},
	 			next: {marginLeft: '190px'}
	 		},
			averagePhotoPosition: {
	 			prev: {marginLeft: '-60px'},
	 			cur: {marginLeft: '0px'},
	 			next: {marginLeft: '-100px'}
	 		},
			smallPhotoPosition: {
	 			prev: {marginLeft: '-60px'},
	 			cur: {marginLeft: '0px'},
	 			next: {marginLeft: '-445px'}
	 		},	
	 		averageTextPosition: {
	 			prev: {
					marginLeft: '233px',
					marginTop: '-303px'	 				
	 			},
	 			cur: {
					marginLeft: '0px',
					marginTop: '0px'	 				
	 			},
	 			next: {
					marginLeft: '-80px',
					marginTop: '-240px'	 				
	 			}
	 		},
			smallTextPosition: {
	 			prev: {
	 				marginLeft: '-60px'	 				
	 			},
	 			cur: {
					marginLeft: '0px'		 				
	 			},
	 			next: {
					marginLeft: '-85px'		 				
	 			}
	 		}
	 	};
		$('#next').bind('click', function (event) {
			let buffer = {
				largePhotoPosition: {
		 			prev: curCollage.largePhotoPosition.cur,
		 			cur: curCollage.largePhotoPosition.next,
		 			next: curCollage.largePhotoPosition.prev
		 		},
				averagePhotoPosition: {
		 			prev: curCollage.averagePhotoPosition.cur,
		 			cur: curCollage.averagePhotoPosition.next,
		 			next: curCollage.averagePhotoPosition.prev
		 		},	 		
				smallPhotoPosition: {
		 			prev: curCollage.smallPhotoPosition.cur,
		 			cur: curCollage.smallPhotoPosition.next,
		 			next: curCollage.smallPhotoPosition.prev
		 		},	 		
		 		averageTextPosition: {
		 			prev: curCollage.averageTextPosition.cur,
		 			cur: curCollage.averageTextPosition.next,
		 			next: curCollage.averageTextPosition.prev
		 		},
				smallTextPosition: {
		 			prev: curCollage.smallTextPosition.cur,
		 			cur: curCollage.smallTextPosition.next,
		 			next: curCollage.smallTextPosition.prev
		 		}				
			};
			curCollage = buffer;
			$('.firstImage').animate({marginLeft: curCollage.largePhotoPosition.cur.marginLeft}, 1000);
			$('.secondImage').animate({marginLeft: curCollage.averagePhotoPosition.cur.marginLeft}, 1000);
			$('.thirdImage').animate({marginLeft: curCollage.smallPhotoPosition.cur.marginLeft}, 1000);
			// $('.large h2').animate({marginRight: curCollage.largeTextPosition.cur.marginRight}, 1000);
			// $('.large #largeText').animate({marginLeft: curCollage.largeTextPosition.cur.marginLeft}, 1000);			
			$('.firstParagraph').animate({
				marginLeft: curCollage.averageTextPosition.cur.marginLeft,
				marginTop: curCollage.averageTextPosition.cur.marginTop
			}, 1000);
			$('.secondParagraph').animate({
				marginLeft: curCollage.smallTextPosition.cur.marginLeft,
			}, 1000);
			// $('.small .signature-body').css('text-align', curCollage.smallTextPosition.cur.text);
			// console.log(curCollage);	
		});
		$('#prev').bind('click', function () {
			let buffer = {
				largePhotoPosition: {
		 			prev: curCollage.largePhotoPosition.next,
		 			cur: curCollage.largePhotoPosition.prev,
		 			next: curCollage.largePhotoPosition.cur
		 		},
				averagePhotoPosition: {
		 			prev: curCollage.averagePhotoPosition.next,
		 			cur: curCollage.averagePhotoPosition.prev,
		 			next: curCollage.averagePhotoPosition.cur
		 		},
				smallPhotoPosition: {
		 			prev: curCollage.smallPhotoPosition.next,
		 			cur: curCollage.smallPhotoPosition.prev,
		 			next: curCollage.smallPhotoPosition.cur
		 		},		 		 		
		 		averageTextPosition: {
		 			prev: curCollage.averageTextPosition.next,
		 			cur: curCollage.averageTextPosition.prev,
		 			next: curCollage.averageTextPosition.cur
		 		},
				smallTextPosition: {
		 			prev: curCollage.smallTextPosition.next,
		 			cur: curCollage.smallTextPosition.prev,
		 			next: curCollage.smallTextPosition.cur
		 		}				
			};
			curCollage = buffer;
			$('.firstImage').animate({marginLeft: curCollage.largePhotoPosition.cur.marginLeft}, 1000);
			$('.secondImage').animate({marginLeft: curCollage.averagePhotoPosition.cur.marginLeft}, 1000);
			$('.thirdImage').animate({marginLeft: curCollage.smallPhotoPosition.cur.marginLeft}, 1000);
			// $('.large h2').animate({marginRight: curCollage.largeTextPosition.cur.marginRight}, 1000);
			// $('.large #largeText').animate({marginLeft: curCollage.largeTextPosition.cur.marginLeft}, 1000);			
			$('.firstParagraph').animate({
				marginLeft: curCollage.averageTextPosition.cur.marginLeft,
				marginTop: curCollage.averageTextPosition.cur.marginTop
			}, 1000);
			$('.secondParagraph').animate({
				marginLeft: curCollage.smallTextPosition.cur.marginLeft,
			}, 1000);
			// $('.small .signature-body').css('text-align', curCollage.smallTextPosition.cur.text);
			// console.log(curCollage);	
		});

		const mySiema = new Siema({
			duration: 1000,
			draggable: false,
			multipleDrag: false,
			loop: true			
		});

		const mobileSiema = new Siema({
			selector: '.mobileSiema',
			duration: 1000,
			draggable: false,
			multipleDrag: false,
			loop: true			
		});

		document.querySelector('#nextSlide').addEventListener('click', function (event) {
			event.preventDefault();
			mobileSiema.next();			
		});

		document.querySelector('#prevSiema').addEventListener('click', function (event) {
			event.preventDefault();
			mySiema.prev();
		});
		document.querySelector('#nextSiema').addEventListener('click', function (event) {
			event.preventDefault();
			mySiema.next();
		});	
	</script>
</html>