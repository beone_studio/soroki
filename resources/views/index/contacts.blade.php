<!DOCTYPE html>
<html>
	<head>
		<title>Сороки</title>
		<link rel="stylesheet" href="/assets/index/css/contacts.css">
		<link rel="stylesheet" href="/assets/index/css/media_contacts.css">
		<link rel="stylesheet" href="/assets/index/css/mobileMenu.css">
		<link rel="stylesheet" href="/assets/index/css/font-awesome.min.css">
		<link rel="shortcut icon" href="/assets/index/img/ico.png">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">			
	</head>
	<body>
		<div class="mobileBlock">
			<div class="mobileMenu">
				<a href="" id="closeMenu" class="closeMenu"><span class="fa fa-times"></span></a>
				<ul class="menu">
					<li><a href="/">Главная</a></li>
					<li><a href="/about">О нас</a></li>
					<!-- <li><a href="/services">Услуги</a></li> -->
					<li><a href="/contacts">Контакты</a></li>
				</ul>				
			</div>
			<div class="header">
				<a id="openMenu" class="fa fa-bars"></a>
				<a class="tel" href="tel:+375296992930">+375 29 699-29-30</a>
			</div>
			<div class="content">
				<h2 class="title">Контакты</h2>
				<p class="description">
					Приглашаем в салон «Сороки» всех, кто хочет получить изысканные услуги по уходу за собой, 
					профессиональное обслуживание и обаятельный современный сервис.
				</p>
				<span class="fa fa-clock-o marker"></span>
			</div>
			<div class="address">
				<p><span class="subtitle">Время работы</span>пн-пт: 08:00 - 21:00<br>сб-вс: 10:00 - 21:00</p>
				<span class="fa fa-map-marker marker"></span>
				<p>г.Минск, ул.Революционная 7</p>
			</div>
			<a class="nodecoration" href="tel:+375296992930">
				<div class="goToServices">
					<div>
						<span class="fa fa-phone"></span>
						<p>Позвонить</p>
					</div>
				</div>
			</a>
		</div>	
		<div class="aboutBlock" id="viewport1">
			<div class="defaultClip">
				<a href="/" class="goMain"><span class="fa fa-arrow-left"></span><br><br>Вернуться <br>на главную</a>
				<div class="leftBlock">
					<ul class="menu" id="menu1">
						<li><a href="" id="menu1Viewport1">Карта и адрес</a></li>
						<li><a href="" id="menu1Viewport2">Связь с нами</a></li>
					</ul>
				</div>				
			</div>
			<div class="container">
				<div class="topElements">
					<a href="/" class="image">
						<img src="/assets/index/img/zelenye_soroki.png" alt="">
					</a>
					<h3>маникюр <span class="and-symbol">&</span> педикюр</h3>
				</div>
				<div class="content">
					<div class="map" id="googleMap"></div>
					<div class="address">
						<h2 class="contactsTitle">Адрес и<br>телефоны</h2>
						<p>г.Минск, ул.Революционная 7<br><br>+375 29 699-29-30<br><br>soroki.by@gmail.com<br><br><span class="subtitle">Время работы</span>пн-пт: 08:00 - 21:00<br>сб-вс: 10:00 - 21:00</p>
					</div>										
				</div>													
			</div>
			<a href="/about" class="color-block right-top">
				<i class="fa fa-info"></i>
				<p class="text">
					О нас
				</p>
			</a>						
		</div>
		<div class="feedback" id="viewport2">
			<div class="defaultClip">
				<a href="/" class="goMain"><span class="fa fa-arrow-left"></span><br><br>Вернуться <br>на главную</a>
				<div class="leftBlock">
					<ul class="menu" id="menu2">
						<li><a href="" id="menu2Viewport1">Карта и адрес</a></li>
						<li><a href="" id="menu2Viewport2">Связь с нами</a></li>
					</ul>
				</div>	
			</div>
			<div class="container mb-20">
				<h2 class="title">
					Свяжитесь<br>с нами
				</h2>
				<form action="/send" role="form" method="post" class="mail">
					<input type="text" name="name" placeholder="Имя" required>
					<input type="text" name="email" placeholder="Эл. почта" required>
					<input type="text" name="number" placeholder="Телефон">
					<textarea name="text" type="text" placeholder="Ваше сообщение"></textarea>
					<button type="submit">Отправить</button>
				</form>
			</div>			
			<div class="footer">
				<div class="soc_networks">
					<a target="_blank" href="https://www.instagram.com/salon_soroki/" class="fa fa-instagram"></a>
					<a target="_blank" href="https://www.facebook.com/salonsoroki/" class="fa fa-facebook"></a>
				</div>
				<p>г.Минск, ул.Революционная 7<br>+375 (29) 699-29-30</p>
				<p>&copy 2008-2017 ООО Дефиле<br>Все права защищены</p>
			</div>
		</div>
		<script src="/assets/index/js/jquery-3.2.1.min.js"></script>
		<script>
			window.onload = function () {
				$('html,body').css('opacity', '1');
				for(let j = 1; j <= 2; j++) {
					for(let i = 1; i <= $('#menu' + j).children().length; i++) {
						$('#menu' + j + 'Viewport' + i).on('click', (event) => {
							event.preventDefault();
							let destination = $('#viewport' + i).position().top;
							$('HTML, BODY').animate({ scrollTop: destination }, 1000);		
						});
					}
				}			
			};	
			$('#openMenu').on('click', function (event) {
				event.preventDefault();
				$(".mobileMenu").css('left') == '0px' ? $(".mobileMenu").css('left','-100%') : $(".mobileMenu").css('left','0');
			});	
			$('#closeMenu').on('click', function (event) {
				event.preventDefault();
				$(".mobileMenu").css('left', '-100%');
			})					
			function myMap() {
				var map = new google.maps.Map(document.getElementById('googleMap'), {
		          center: {lat: 53.9027374, lng: 27.5518991},
		          zoom: 17,
		          styles: [
								{
										"featureType": "administrative",
										"elementType": "all",
										"stylers": [
												{
														"saturation": "-100"
												}
										]
								},
								{
										"featureType": "administrative.province",
										"elementType": "all",
										"stylers": [
												{
														"visibility": "off"
												}
										]
								},
								{
										"featureType": "landscape",
										"elementType": "all",
										"stylers": [
												{
														"saturation": -100
												},
												{
														"lightness": 65
												},
												{
														"visibility": "on"
												}
										]
								},
								{
										"featureType": "poi",
										"elementType": "all",
										"stylers": [
												{
														"saturation": -100
												},
												{
														"lightness": "50"
												},
												{
														"visibility": "simplified"
												}
										]
								},
								{
										"featureType": "road",
										"elementType": "all",
										"stylers": [
												{
														"saturation": "-100"
												}
										]
								},
								{
										"featureType": "road.highway",
										"elementType": "all",
										"stylers": [
												{
														"visibility": "simplified"
												}
										]
								},
								{
										"featureType": "road.arterial",
										"elementType": "all",
										"stylers": [
												{
														"lightness": "30"
												}
										]
								},
								{
										"featureType": "road.local",
										"elementType": "all",
										"stylers": [
												{
														"lightness": "40"
												}
										]
								},
								{
										"featureType": "transit",
										"elementType": "all",
										"stylers": [
												{
														"saturation": -100
												},
												{
														"visibility": "simplified"
												}
										]
								},
								{
										"featureType": "water",
										"elementType": "geometry",
										"stylers": [
												{
														"hue": "#ffff00"
												},
												{
														"lightness": -25
												},
												{
														"saturation": -97
												}
										]
								},
								{
										"featureType": "water",
										"elementType": "labels",
										"stylers": [
												{
														"lightness": -25
												},
												{
														"saturation": -100
												}
										]
								}
						]
		        });
						var marker = new google.maps.Marker({
							position: {lat: 53.902737, lng: 27.552446},
							map: map,
							title: 'Сороки'
						});
			}
		</script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCJcKXsZrXwdbXx2CM08n3AHl9frIHxU1w&callback=myMap"></script>
	</body>
</html>