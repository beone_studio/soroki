<!DOCTYPE html>
<html>
	<head>
		<title>Сороки</title>
		<link rel="stylesheet" href="/assets/index/css/depilation.css">
		<link rel="stylesheet" href="/assets/index/css/media_inner.css">
		<link rel="stylesheet" href="/assets/index/css/mobileMenu.css">
		<link rel="stylesheet" href="/assets/index/css/font-awesome.min.css">	
		<link rel="shortcut icon" href="/assets/index/img/ico.png">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">	
	</head>
	<body>
		<div class="mobileBlock">
			<div class="mobileMenu">
				<a href="" id="closeMenu" class="closeMenu"><span class="fa fa-times"></span></a>
				<ul class="menu">
					<li><a href="/">Главная</a></li>
					<li><a href="/about">О нас</a></li>
					<li><a href="/services">Услуги</a></li>
					<li><a href="/contacts">Контакты</a></li>
				</ul>				
			</div>
			<div class="header">
				<a id="openMenu" class="fa fa-bars"></a>
				<a class="tel" href="tel:+375296992930">+375 29 699-29-30</a>
			</div>
			<div class="content">
				<h2 class="title">Маникюр</h2>
				<p class="description">
					Мы готовы предложить один из лучших сервисов в центре Минска.
					Уютная теплая атмосфера и качественный маникюр, который сумел впечатлить уже множество клиентов. 
					В “Сороках” о ваших ногтях заботятся высоквалифицированные специалисты с многолетним опытом. 
					Ждем Вас в нашем "гнездышке".
				</p>
				<!-- <a href="#" id="nextSlide" class="nextSlide"><span class="fa fa-arrow-right"></span></a> -->
			</div>
			<div class="procedures">
				<div class="table-top">
					<span class="fa fa-usd"></span>Цены на услуги
				</div>			
				<div class="procedure" id="procedure1">
					<p class="procedureName">Классический маникюр</p>
					<div class="time-and-price">
						<i class="time">60</i><span>мин</span>
						<i class="price">20 <p class="cent">00</p></i><span>руб.</span>
					</div>
					<!-- <p class="booking" style="display: none">+375 29 699-29-30</p> -->
				</div>
				<div class="procedure" id="procedure2">
					<p class="procedureName">Европейский маникюр</p>
					<div class="time-and-price">
						<i class="time">60</i><span>мин</span>
						<i class="price">23 <p class="cent">00</p></i><span>руб.</span>
					</div>
					<!-- <p class="booking" style="display: none">+375 29 699-29-30</p> -->
				</div>	
				<div class="procedure" id="procedure3">
					<p class="procedureName">Городской маникюр</p>
					<div class="time-and-price">
						<i class="time">45</i><span>мин</span>
						<i class="price">17 <p class="cent">00</p></i><span>руб.</span>
					</div>
					<!-- <p class="booking" style="display: none">+375 29 699-29-30</p> -->
				</div>																			
			</div>
		</div>	
		<div class="aboutBlock" id="viewport1">
			<div class="defaultClip">
				<a href="/" class="goMain"><span class="fa fa-arrow-left"></span><br><br>Вернуться <br>на главную</a>
				<div class="leftBlock">
					<ul class="menu" id="menu1">
						<li><a href="" id="menu1Viewport1">Маникюр</a></li>
						<li><a href="" id="menu1Viewport2">Цены за услуги</a></li>
					</ul>
				</div>
			</div>
			<div class="container">
				<div class="topElements">
					<div class="image">
						<img src="/assets/index/img/zelenye_soroki.png" alt="">
					</div>
					<h3>маникюр & педикюр</h3>
				</div>
				<div class="emptyRow"></div>
				<div class="content">
					<div class="depilationInMinsk">
						<h2 class="title">Маникюр</h2>
						<p class="depilationDescription">
							Мы готовы предложить один из лучших сервисов в центре Минска.
							Уютная теплая атмосфера и качественный маникюр, который сумел впечатлить уже множество клиентов. 
							В “Сороках” о Ваших ногтях заботятся высоквалифицированные специалисты с многолетним опытом. 
							Ждем Вас в нашем "гнездышке".
						</p>
					</div>
					<div class="advices">
						<div class="advice">
							<i class="number">1</i>
							<p>Делай маникюр одновременно с педикюром, мы работаем в 4 руки. Экономь свое время.</p>
						</div>
						<div class="advice">
							<i class="number">2</i>
							<p>Наши специалисты всегда расскажут про тренды сезона в дизайне ногтей</p>
						</div>
						<div class="advice">
							<i class="number">3</i>
							<p>Только самые качественные материалы и современное оборудование </p>
						</div>
						<div class="advice">
							<i class="number">4</i>
							<p>Подарите своим ногтям красоту и уход, достойные восхищения</p>
						</div>	
					</div>					
				</div>													
			</div>
			<div href="" class="color-block right-top">
				<i class="fa fa-phone"></i>
				<p class="text">
					+375 29 699-29-30
				</p>
			</div>						
		</div>	
		<div class="aboutBlock depilationBottomBlock" id="viewport2">
			<div class="whiteClip">
				<a href="/" class="goMain"><span class="fa fa-arrow-left"></span><br><br>Вернуться <br>на главную</a>
				<div class="leftBlock">
					<ul class="menu" id="menu2">
						<li><a href="" id="menu2Viewport1">Маникюр</a></li>
						<li><a href="" id="menu2Viewport2">Цены за услуги</a></li>
					</ul>
				</div>
			</div>
			<div class="container">
				<h2 class="title depilationViewport2Header">Цены за<br>услуги</h2>
				<div class="content">
					<div class="procedures">
						<div class="procedure">
							<p class="procedureName procedureHeader">Название процедуры</p>
							<p class="procedureHeader timeHeader">Время</p>
							<p class="procedureHeader priceHeader">Цена</p>						
						</div>
						<div class="procedure" id="procedure1">
							<p class="procedureName">Классический маникюр</p>
							<p class="time">60 мин.</p>
							<p class="price">23 руб. 00 к.</p>
							<p class="booking" style="display: none">+375 29 699-29-30</p>
						</div>
						<div class="procedure" id="procedure2">
							<p class="procedureName">Европейский маникюр</p>
							<p class="time">60 мин.</p>
							<p class="price">23 руб. 00 к.</p>
							<p class="booking" style="display: none">+375 29 699-29-30</p>
						</div>	
						<div class="procedure" id="procedure3">
							<p class="procedureName">Городской маникюр</p>
							<p class="time">45 мин.</p>
							<p class="price">17 руб. 00 к.</p>
							<p class="booking" style="display: none">+375 29 699-29-30</p>
						</div>																			
					</div>
				</div>
			</div>	
			<div class="footer">
				<div class="soc_networks">
					<a target="_blank" href="https://www.instagram.com/salon_soroki/" class="fa fa-instagram"></a>
					<a target="_blank" href="https://www.facebook.com/salonsoroki/" class="fa fa-facebook"></a>
				</div>
				<p>г.Минск, ул.Революционная 7<br>+375 (29) 699-29-30</p>
				<p>&copy 2008-2017 ООО Дефиле<br>Все права защищены</p>
			</div>					
		</div>			
	</body>
	<script src="/assets/index/js/jquery-3.2.1.min.js"></script>
	<script>
		window.onload = function () {
			$('html,body').css('opacity', '1');
			for(let j = 1; j <= 2; j++) {
				for(let i = 1; i <= $('#menu' + j).children().length; i++) {
					$('#menu' + j + 'Viewport' + i).on('click', (event) => {
						event.preventDefault();
						let destination = $('#viewport' + i).position().top;
						$('HTML, BODY').animate({ scrollTop: destination }, 1000);		
					});
				}
			}
			$('#openMenu').on('click', function (event) {
				event.preventDefault();
				$(".mobileMenu").css('left') == '0px' ? $(".mobileMenu").css('left','-100%') : $(".mobileMenu").css('left','0');
			});	
			$('#closeMenu').on('click', function (event) {
				event.preventDefault();
				$(".mobileMenu").css('left', '-100%');
			})						
		};		
	</script>
</html>