<!DOCTYPE html>
<html>
	<head>
		<title>Сороки</title>
		<link rel="stylesheet" href="/assets/index/css/depilation.css">
		<link rel="stylesheet" href="/assets/index/css/media_inner.css">
		<link rel="stylesheet" href="/assets/index/css/mobileMenu.css">
		<link rel="stylesheet" href="/assets/index/css/font-awesome.min.css">	
		<link rel="shortcut icon" href="/assets/index/img/ico.png">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">	
	</head>
	<body>
		<div class="mobileBlock">
			<div class="mobileMenu">
				<a href="" id="closeMenu" class="closeMenu"><span class="fa fa-times"></span></a>
				<ul class="menu">
					<li><a href="/">Главная</a></li>
					<li><a href="/about">О нас</a></li>
					<li><a href="/services">Услуги</a></li>
					<li><a href="/contacts">Контакты</a></li>
				</ul>				
			</div>
			<div class="header">
				<a id="openMenu" class="fa fa-bars"></a>
				<a class="tel" href="tel:+375296992930">+375 29 699-29-30</a>
			</div>
			<div class="content">
				<h2 class="title">Наращивание ногтей</h2>
				<p class="description">
					В “Сороках” мы используем самые современные и безопасные технологии по наращиванию ногтей. 
					Все это позволяет защищать ноготь, обеспечивать глубокий насыщенный однородный цвет покрытия, 
					а ультрасовременная формула обеспечивает длительную носку и изумительный блеск.
				</p>
				<!-- <a href="#" id="nextSlide" class="nextSlide"><span class="fa fa-arrow-right"></span></a> -->
			</div>
			<div class="procedures">
				<div class="table-top">
					<span class="fa fa-usd"></span>Цены на услуги
				</div>			
				<div class="procedure" id="procedure16">
					<p class="procedureName">Наращивание ногтей</p>
					<div class="time-and-price">
						<span>От</span><i class="time">40</i><span>мин</span>
						<i class="price">63 <p class="cent">00</p></i><span>руб.</span>
					</div>
					<!-- <p class="booking" style="display: none">+375 29 699-29-30</p> -->
				</div>																		
			</div>			
		</div>	
		<div class="aboutBlock" id="viewport1">
			<div class="defaultClip">
				<a href="/" class="goMain"><span class="fa fa-arrow-left"></span><br><br>Вернуться <br>на главную</a>
				<div class="leftBlock">
					<ul class="menu" id="menu1">
						<li><a href="" id="menu1Viewport1">Наращивание ногтей</a></li>
						<li><a href="" id="menu1Viewport2">Цены за услуги</a></li>
					</ul>
				</div>
			</div>
			<div class="container">
				<div class="topElements">
					<div class="image">
						<img src="/assets/index/img/zelenye_soroki.png" alt="">
					</div>
					<h3>маникюр & педикюр</h3>
				</div>
				<div class="emptyRow"></div>
				<div class="content">
					<div class="depilationInMinsk">
						<h2 class="title">Наращивание<br>ногтей</h2>
						<p class="depilationDescription">
							В “Сороках” мы используем самые современные и безопасные технологии по наращиванию ногтей. 
							Все это позволяет защищать ноготь, обеспечивать глубокий насыщенный однородный цвет покрытия, 
							а ультрасовременная формула обеспечивает длительную носку и изумительный блеск.
						</p>
					</div>
					<div class="advices">
						<div class="advice">
							<i class="number">1</i>
							<p>Профессиональное наращивание позволит получить естественную красоту</p>
						</div>
						<div class="advice">
							<i class="number">2</i>
							<p>Наращивание ногтей значительно упрощает уход за ними</p>
						</div>
						<div class="advice">
							<i class="number">3</i>
							<p>Данная процедура не несет вреда для натуральных ногтей</p>
						</div>
						<div class="advice">
							<i class="number">4</i>
							<p>Доверьтесь опытному мастеру и насладитесь результатом</p>
						</div>	
					</div>					
				</div>													
			</div>
			<div href="" class="color-block right-top">
				<i class="fa fa-phone"></i>
				<p class="text">
					+375 29 699-29-30
				</p>
			</div>						
		</div>	
		<div class="aboutBlock depilationBottomBlock" id="viewport2">
			<div class="whiteClip">
				<a href="/" class="goMain"><span class="fa fa-arrow-left"></span><br><br>Вернуться <br>на главную</a>
				<div class="leftBlock">
					<ul class="menu" id="menu2">
						<li><a href="" id="menu2Viewport1">Наращивание ногтей</a></li>
						<li><a href="" id="menu2Viewport2">Цены за услуги</a></li>
					</ul>
				</div>
			</div>
			<div class="container">
				<h2 class="title depilationViewport2Header">Цены на<br>шугаринг</h2>
				<div class="content">
					<div class="procedures">
						<div class="procedure">
							<p class="procedureName procedureHeader">Название процедуры</p>
							<p class="procedureHeader timeHeader">Время</p>
							<p class="procedureHeader priceHeader">Цена</p>						
						</div>
						<div class="procedure" id="procedure16">
							<p class="procedureName">Наращивание ногтей</p>
							<p class="time">От 40 мин.</p>
							<p class="price">63 руб. 00 к.</p>
							<p class="booking" style="display: none">+375 29 699-29-30</p>
						</div>																		
					</div>
				</div>
			</div>	
			<div class="footer">
				<div class="soc_networks">
					<a target="_blank" href="https://www.instagram.com/salon_soroki/" class="fa fa-instagram"></a>
					<a target="_blank" href="https://www.facebook.com/salonsoroki/" class="fa fa-facebook"></a>
				</div>
				<p>г.Минск, ул.Революционная 7<br>+375 (29) 699-29-30</p>
				<p>&copy 2008-2017 ООО Дефиле<br>Все права защищены</p>
			</div>					
		</div>			
	</body>
	<script src="/assets/index/js/jquery-3.2.1.min.js"></script>
	<script>
		window.onload = function () {
			$('html,body').css('opacity', '1');
			for(let j = 1; j <= 2; j++) {
				for(let i = 1; i <= $('#menu' + j).children().length; i++) {
					$('#menu' + j + 'Viewport' + i).on('click', (event) => {
						event.preventDefault();
						let destination = $('#viewport' + i).position().top;
						$('HTML, BODY').animate({ scrollTop: destination }, 1000);		
					});
				}
			}
			$('#openMenu').on('click', function (event) {
				event.preventDefault();
				$(".mobileMenu").css('left') == '0px' ? $(".mobileMenu").css('left','-100%') : $(".mobileMenu").css('left','0');
			});	
			$('#closeMenu').on('click', function (event) {
				event.preventDefault();
				$(".mobileMenu").css('left', '-100%');
			})						
		};		
	</script>
</html>