<!DOCTYPE html>
<html>
<head>
	<title>Сороки</title>
	<link rel="stylesheet" href="/assets/index/css/main.css">
	<link rel="stylesheet" href="/assets/index/css/media_main.css">
	<link rel="stylesheet" href="/assets/index/css/mobileMenu.css">
	<link rel="stylesheet" href="/assets/index/css/font-awesome.min.css">
	<link rel="shortcut icon" href="/assets/index/img/ico.png">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
	@if(Session::has('flash_message'))
	  <div id="flash_message">
	    <span class="fa fa-check" style="margin-right: 10px;font-size: 24px;"></span>{{ Session::get('flash_message') }}
	  </div>
	@endif
	<div class="topBlock">
		<div class="mobile">
			<div class="mobileMenu">
				<a href="" id="closeMenu" class="closeMenu"><span class="fa fa-times"></span></a>
				<ul class="menu">
					<li><a href="/">Главная</a></li>
					<li><a href="/about">О нас</a></li>
					<!-- <li><a href="">Услуги</a></li> -->
					<li><a href="/contacts">Контакты</a></li>
				</ul>				
			</div>			
			<img class="mob" src="/assets/index/img/mob.jpg">
			<div class="header">
				<a id="openMenu" class="fa fa-bars"></a>
				<a class="tel" href="tel:+375296992930">+375 29 699-29-30</a>
			</div>
			<h1>Трещим и чистим<br>перышки</h1>
			<a class="nodecoration" href="/about">
				<div class="goToServices">
					<div>
						<span class="fa fa-arrow-down"></span>
						<p>Узнать подробнее</p>
					</div>
				</div>
			</a>
		</div>
		
		<div class="siema">
			<img id="image1" src="/assets/index/img/1.jpg" alt="">
			<img id="image2" src="/assets/index/img/2.jpg" alt="">
			<img id="image3" src="/assets/index/img/main_slider_armchaires.jpg" alt="">
			<img id="image4" src="/assets/index/img/4.jpg" alt="">
		</div>
		
		<div class="whiteClip">
			<div class="leftBlock">
				<ul class="menu">
					<li><a href="/">Главная</a></li>
					<li><a href="/about">О нас</a></li>
					<li><a id="services1" href="">Услуги</a></li>
					<li><a href="/contacts">Контакты</a></li>
				</ul>
			</div>
		</div>
		<a href="tel:+375296992930" class="telefon">
			<strong>+375 29 699-29-30</strong>
		</a>		
		<div class="container topContainer">
			<div class="topElements white">
				<a href="/" class="topImage">
					<img src="/assets/index/img/belyesoroki.png" alt="">
				</a>
				<h3>маникюр <span class="and-symbol">&</span> педикюр</h3>
			</div>
			<div class="content">
				<h1>Трещим и чистим<br>перышки</h1>
				<!-- <div class="paragraph"> -->
					<!-- <h4>Приглашаем всех!</h4>
					<p>
						Приглашаем в салон «Сороки» всех, кто хочет получить изысканные услуги по уходу за собой, 
						профессиональное обслуживание и обаятельный современный сервис.
					</p> -->
				<!-- </div> -->
			</div>
			<div class="slideButtons">
				<a class="prev fa fa-arrow-left" href=""></a>
				<p class="pages"></p>
				<a class="next  fa fa-arrow-right" href=""></a>
			</div>
		</div>
		<a href="/about" class="color-block right-bottom hover">
			<i class="fa fa-hand-o-down"></i>
			<p class="text">
				Узнать больше
			</p>
		</a>
	</div>
	<div class="aboutBlock">
		<div class="defaultClip">
			<div class="leftBlock">
				<ul class="menu">
					<li><a href="/">Главная</a></li>
					<li><a href="/about">О нас</a></li>
					<li><a id="services2" href="">Услуги</a></li>
					<li><a href="/contacts">Контакты</a></li>
				</ul>
			</div>
		</div>		
		<div class="container">
			<a href="tel:+375296992930" class="telefon">
				<strong>+375 29 699-29-30</strong>
			</a>		
			<div class="topElements">
				<a href="/" class="image">
					<img src="/assets/index/img/zelenye_soroki.png" alt="">
				</a>
				<h3>маникюр <span class="and-symbol">&</span> педикюр</h3>
				<!-- <strong>+375 29 699-29-30</strong> -->
			</div>
			<div class="content" style="font-size: 0; width: 87%;">
					<div class="firstBlock">
						<div class="firstImage">
							<img src="/assets/index/img/chears.jpg" alt="">
						</div>
						<div class="firstParagraph">
							<!-- <p class="firstParagraph"> -->
							<p>
								Уникальность салона начинается с его местоположения - старейшее здание в историческом квартале в центре Минска - и заканчивается личностью каждого сотрудника. Создатели постарались сохранить «экологичность» исторического места - то есть соединили экостиль с модными дизайнерскими решениями, - и это вполне отвечает миксовому дизайну 21 века, в котором отлично сочетаются старинная архитектура и современность.	
							</p>					
							<!-- </p> -->
						</div>
					</div>
					<div class="secondBlock">
						<div class="secondParagraph">
							<p>
								Главная цель салона - создать атмосферу и обслуживание класса люкс для постоянно бегущих и быстроживущих современных минчан.
							</p>
						</div>					
						<div class="secondImage">
							<img src="/assets/index/img/lamps.jpg" alt="">
						</div>												
					</div>
					<div class="thirdBlock">
						<div class="text">
							<h2 class="title">Почему Сороки</h2>
							<div class="thirdParagraph">
								<p>
									"Сороки" - это ассоциативный образ встречи подруг, которые умеют совмещать приятное с полезным: уход за собой и веселый светский разговор. К тому же сорока - умная птица и "очень женщина" - любит все яркое, блестящее, видит и узнает себя в зеркале, а в восточной культуре эта птица является символом счастья и приносит радость.
								</p>
								<!-- <div class="buttons">
									<button id="prev"><span class="fa fa-arrow-left"></span></button>
									<button id="next"><span class="fa fa-arrow-right"></span></button>
								</div> -->
							</div>															
						</div>
						<div class="thirdImage">
							<img src="/assets/index/img/employee.jpg" alt="">
						</div>						
					</div>				
				<!-- <div class="collage">
					<div class="signature large">
						<h2 class="title photoTitle">О нас</h2>
						<p class="signature-body">
							Первого октября 2017 года по улице Революционной, 7 в городе Минске открылся новый салон «Сороки». 
							Такое название многим покажется удивительным. «Сороки» – это ассоциативный образ встречи подруг, 
							которые умеют совмещать приятное с полезным: уход за собой и веселый светский разговор.
						</p>							
					</div>
					<div class="largePhoto">
						<img src="/assets/index/img/chears.jpg" alt="">
						<div class="signature average">
							<p class="signature-body">Сорока - умная птица и весьма «женственная». Она любит все яркое, блестящее, видит и узнает себя в зеркале, а в восточной культуре эта птица является символом счастья и приносит радость.</p>
						</div>
						<div class="signature small">
							<p class="signature-body">В нашем салоне клиент окутан уходом, теплом и заботой персонала.</p>
						</div>													
						<div class="averagePhoto">
							<img src="/assets/index/img/lak.jpg" alt="">						
						</div>
						<div class="smallPhoto">
							<img src="/assets/index/img/soroki.jpg" alt="">
						</div>
					</div>
				</div> -->
			</div>
		</div>
	</div>
	<!-- <div class="colorClip" style="margin-top: -30vh;">
		<div>
			<div class="leftBlock">
				<ul class="menu">
					<li><a href="/">Главная</a></li>
					<li><a href="/about">О нас</a></li>
					<li><a href="/services">Услуги</a></li>
					<li><a href="/contacts">Контакты</a></li>
				</ul>
			</div>
		</div>
	</div> -->
	<div class="serviceBlock" id="serviceDestination">
		<div class="defaultClip">
			<div class="leftBlock">
				<ul class="menu">
					<li><a href="/">Главная</a></li>
					<li><a href="/about">О нас</a></li>
					<li><a id="services3" href="">Услуги</a></li>
					<li><a href="/contacts">Контакты</a></li>
				</ul>
			</div>
		</div>
		<div class="container">
			<div class="textContainer">
				<h2 class="title serviceTitle">Услуги</h2>
				<p class="infotext">
					Основываясь на современных трендах повседневной городской жизни, 
					рады предложить каждому нашему посетителю в «Сороках» ряд услуг, 
					после которых он почувствует прилив сил. Наши мастера своей работой не оставят 
					никого равнодушным и порадуют самого предвзятого эксперта.
					Подарите себе кучу незабываемых эмоций и ощущений. В нашем салоне каждому захочется "летать".
				</p>
			</div>
			<div class="procedures">
				<div class="procedure">
					<p class="procedureName procedureHeader">Название процедуры</p>
					<p class="timeHeader procedureHeader">Время</p>
					<p class="priceHeader procedureHeader">Цена</p>						
				</div>			
				@foreach($services as $service)
				<div class="procedure">
					<p class="procedureName">{{$service->name}}</p>
					<p class="time">{{$service->time}} мин.</p>
					<p class="price">{{$service->price}} руб. 00 к.</p>
					<a href="tel:+375296992930" class="booking" style="display: none">+375 29 699-29-30</a>
				</div>				
				@endforeach
			</div>				
		</div>
	</div>
	<div class="feedback">
		<div class="defaultClip">
			<div class="leftBlock">
				<ul class="menu">
					<li><a href="/">Главная</a></li>
					<li><a href="/about">О нас</a></li>
					<li><a id="services4" href="">Услуги</a></li>
					<li><a href="/contacts">Контакты</a></li>
				</ul>
			</div>
		</div>
		<div class="container">
			<h2 class="title">
				Свяжитесь<br>с нами
			</h2>
			<form action="/send" role="form" method="post" class="mail">
				<input type="text" name="name" placeholder="Имя" required>
				<input type="text" name="email" placeholder="Эл. почта" required>
				<input type="text" name="number" placeholder="Телефон">
				<textarea name="text" type="text" placeholder="Ваше сообщение"></textarea>
				<button type="submit">Отправить</button>
			</form>
			<div class="content">
				<div class="map" id="googleMap"></div>
				<div class="address">
					<h2 class="contactsTitle">Контакты</h2>
					<p>г.Минск, ул.Революционная 7<br><br>+375 29 699-29-30<br><br>soroki.by@gmail.com<br><br><span class="subtitle">Время работы</span>пн-пт: 08:00 - 21:00<br>сб-вс: 10:00 - 21:00</p>
				</div>										
			</div>
		</div>
		<div class="footer">
			<div class="soc_networks">
				<a target="_blank" href="https://www.instagram.com/salon_soroki/" class="fa fa-instagram"></a>
				<a target="_blank" href="https://www.facebook.com/salonsoroki/" class="fa fa-facebook"></a>
			</div>
			<p>г.Минск, ул.Революционная 7<br>+375 (29) 699-29-30</p>
			<p>&copy 2008-2017 ООО Дефиле<br>Все права защищены</p>
		</div>
	</div>
</body>

<script src="/assets/index/js/siema.min.js"></script>
<script src="/assets/index/js/jquery-3.2.1.min.js"></script>
<script>	
	$('#flash_message').delay(2000).slideUp(400);	
	function myMap() {
		var map = new google.maps.Map(document.getElementById('googleMap'), {
			center: {lat: 53.9027374, lng: 27.5518991},
			zoom: 17,
			styles: [
						{
							"featureType": "administrative",
							"elementType": "all",
							"stylers": [
									{
											"saturation": "-100"
									}
							]
						},
						{
							"featureType": "administrative.province",
							"elementType": "all",
							"stylers": [
									{
											"visibility": "off"
									}
							]
						},
						{
							"featureType": "landscape",
							"elementType": "all",
							"stylers": [
									{
											"saturation": -100
									},
									{
											"lightness": 65
									},
									{
											"visibility": "on"
									}
							]
						},
						{
							"featureType": "poi",
							"elementType": "all",
							"stylers": [
									{
											"saturation": -100
									},
									{
											"lightness": "50"
									},
									{
											"visibility": "simplified"
									}
							]
						},
						{
							"featureType": "road",
							"elementType": "all",
							"stylers": [
									{
											"saturation": "-100"
									}
							]
						},
						{
							"featureType": "road.highway",
							"elementType": "all",
							"stylers": [
									{
											"visibility": "simplified"
									}
							]
						},
						{
							"featureType": "road.arterial",
							"elementType": "all",
							"stylers": [
									{
											"lightness": "30"
									}
							]
						},
						{
							"featureType": "road.local",
							"elementType": "all",
							"stylers": [
									{
											"lightness": "40"
									}
							]
						},
						{
							"featureType": "transit",
							"elementType": "all",
							"stylers": [
									{
											"saturation": -100
									},
									{
											"visibility": "simplified"
									}
							]
						},
						{
							"featureType": "water",
							"elementType": "geometry",
							"stylers": [
									{
											"hue": "#ffff00"
									},
									{
											"lightness": -25
									},
									{
											"saturation": -97
									}
							]
						},
						{
							"featureType": "water",
							"elementType": "labels",
							"stylers": [
									{
											"lightness": -25
									},
									{
											"saturation": -100
									}
							]
						}
				]
		});
		var marker = new google.maps.Marker({
			position: {lat: 53.902737, lng: 27.552446},
			map: map,
			title: 'Сороки'
		});
	}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCJcKXsZrXwdbXx2CM08n3AHl9frIHxU1w&callback=myMap"></script>
<script>
$('#openMenu').on('click', function (event) {
	event.preventDefault();
	$(".mobileMenu").css('left') == '0px' ? $(".mobileMenu").css('left','-100%') : $(".mobileMenu").css('left','0');
});	
$('#closeMenu').on('click', function (event) {
	event.preventDefault();
	$(".mobileMenu").css('left', '-100%');
})
let curImageNum = 1;
var numOfPhotosInSlider;
const idStartName = 'image';
/*need to fix*/
var curPage = function (id) {
	let siemaItem;
	if(id.substring(5) === '0') {
		siemaItem = document.getElementById('image' + numOfPhotosInSlider);
	} else { 
		if(id.substring(5) === '5') {
			siemaItem = document.getElementById('image1');
		} else {
			siemaItem = document.getElementById(id);
		}
	}
	curImageNum = $('img').index(siemaItem);
	return curImageNum;
}
window.onload = function () {
	$('html,body').css('opacity', '1');
	for(let i = 1; i <= 4; i++) {
		$('#services' + i).on('click', function(event) {
			event.preventDefault();
			let destination = $('#serviceDestination').position().top;
			$('HTML, BODY').animate({ scrollTop: destination }, 1000)		
		});
	}
	numOfPhotosInSlider = $('.siema img').length;
	$('.pages').text(curPage(idStartName + curImageNum) + ' / ' + numOfPhotosInSlider);
};

const mySiema = new Siema({
	duration: 2000,
	draggable: false,
	multipleDrag: false,
	loop: true
});
document.querySelector('.prev').addEventListener('click', function (event) {
	event.preventDefault();
	$('.pages').text(curPage(idStartName + (curImageNum - 1)) + ' / ' + numOfPhotosInSlider);
	mySiema.prev();
});
document.querySelector('.next').addEventListener('click', function (event) {
	event.preventDefault();
	$('.pages').text(curPage(idStartName + (curImageNum + 1)) + ' / ' + numOfPhotosInSlider);
	mySiema.next();
});
</script>
</html>