<!DOCTYPE html>
<html>
	<head>
		<title>Сороки</title>
		<link rel="stylesheet" href="/assets/index/css/manicure.css">
		<link rel="stylesheet" href="/assets/index/css/media_services.css">
		<link rel="stylesheet" href="/assets/index/css/mobileMenu.css">
		<link rel="stylesheet" href="/assets/index/css/font-awesome.min.css">
		<link rel="shortcut icon" href="/assets/index/img/ico.png">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">		
	</head>
	<body>
		<div class="mobileBlock">
			<div class="mobileMenu">
				<a href="" id="closeMenu" class="closeMenu"><span class="fa fa-times"></span></a>
				<ul class="menu">
					<li><a href="/">Главная</a></li>
					<li><a href="/about">О нас</a></li>
					<li><a href="/services">Услуги</a></li>
					<li><a href="/contacts">Контакты</a></li>
				</ul>				
			</div>
			<div class="header">
				<a id="openMenu" class="fa fa-bars"></a>
				<a class="tel" href="tel:+375296992930">+375 29 699-29-30</a>
			</div>
			<div class="content">
				<h2 class="title">Услуги</h2>
				<p class="description">
					В “Сороках” собраны одни из лучших мастеров своего дела, 
					которые постоянно повышают свою квалификацию и изучают новые технологии данного направления. 
					Мы используем только профессиональное оборудование и сертифицированную косметику. 
					Кроме привычных всем процедур, у нас вы сможете узнать много нового и попробовать это на себе. 
					Сотни довольных клиентов ежедневно доверяют нам свою красоту.
				</p>
				<!-- <a href="#" id="nextSlide" class="nextSlide"><span class="fa fa-venus-mars"></span></a> -->
			</div>
			<div class="services">
				<a href="/services/manikyur" class="service">
					<div class="icon">
						<p>Mн</p>
					</div>
					<p>Маникюр</p>
				</a>
				<a href="/services/pedikyur" class="service">
					<div class="icon">
						<p>Пд</p>
					</div>
					<p>Педикюр</p>
				</a>
				<a href="/services/pokrytie" class="service">
					<div class="icon">
						<p>Гл</p>
					</div>
					<p>Покрытие <br> гель-лаком</p>
				</a>
				<a href="/services/parafin" class="service">
					<div class="icon">
						<p>Пт</p>
					</div>
					<p>Парафино-<br>терапия</p>
				</a>			
				<a href="/services/brovi" class="service">
					<div class="icon">
						<p>Бр</p>
					</div>
					<p>Брови и <br> ресницы</p>
				</a>
				<a href="/services/narashhivanie" class="service">
					<div class="icon">
						<p>Нн</p>
					</div>
					<p>Наращивание <br> ногтей</p>
				</a>
			</div>
			<a class="nodecoration" href="/contacts">
				<div class="goToServices">
					<div>
						<span class="fa fa-arrow-down"></span>
						<p>Контакты</p>
					</div>
				</div>
			</a>
		</div>		
		<div class="aboutBlock" id="viewport1">
			<div class="defaultClip">
				<a href="/" class="goMain"><span class="fa fa-arrow-left"></span><br><br>Вернуться <br>на главную</a>
				<div class="leftBlock">
					<ul class="menu" id="menu1">
						<li><a href="" id="menu1Viewport1">Популярные услуги</a></li>
						<li><a href="" id="menu1Viewport2">Наши задачи</a></li>
						<li><a href="" id="menu1Viewport3">Список всех услуг</a></li>
					</ul>
				</div>
			</div>
			<div class="container">
				<div class="topElements">
					<div class="image">
						<img src="/assets/index/img/zelenye_soroki.png" alt="">
					</div>
					<h3>маникюр & педикюр</h3>
				</div>
				<div class="content" style="margin-top: 15vh;">
					<div class="benefits">
						<a href="/services/manikyur" class="benefit">
							<div class="icon">
								<p>Mн</p>
							</div>
							<p>Маникюр</p>
						</a>
						<div class="manicureInMinsk">
							<h2 class="title">Популярные услуги</h2>
							<p>
								В “Сороках” собраны одни из лучших мастеров своего дела, 
								которые постоянно повышают свою квалификацию и изучают новые технологии данного направления. 
								Мы используем только профессиональное оборудование и сертифицированную косметику. 
								Кроме привычных всем процедур, у нас вы сможете узнать много нового и попробовать это на себе. 
								Сотни довольных клиентов ежедневно доверяют нам свою красоту.
							</p>
						</div>
						<a href="/services/pokrytie" class="benefit">
							<div class="icon">
								<p>Гл</p>
							</div>
							<p>Покрытие <br> гель-лаком</p>
						</a>
						<a href="/services/pedikyur" class="benefit">
							<div class="icon">
								<p>Пд</p>
							</div>
							<p>Педюкюр</p>
						</a>
					</div>					
				</div>													
			</div>
			<a href="/contacts" class="color-block right-top">
				<i class="fa fa-phone"></i>
				<p class="text">
					Контакты
				</p>
			</a>						
		</div>
		<div class="aboutBlock" id="viewport2">
			<div class="defaultClip">
				<a href="/" class="goMain"><span class="fa fa-arrow-left"></span><br><br>Вернуться <br>на главную</a>
				<div class="leftBlock">
					<ul class="menu" id="menu2">
					<li><a href="" id="menu2Viewport1">Популярные услуги</a></li>
					<li><a href="" id="menu2Viewport2">Наши задачи</a></li>
					<li><a href="" id="menu2Viewport3">Список всех услуг</a></li>
					</ul>
				</div>
			</div>
			<div class="container">
				<div class="content">
					<div class="largePhoto">
						<h2 class="title photoTitle">Наши<br>задачи</h2>
						<img src="/assets/index/img/1chear.jpg" alt="">	
						<div class="signature large">
							<p class="signature-head">Комфорт</p>
							<p class="signature-body">
								Одна из наших целей – создать комфортную атмосферу  
								для постоянно бегущих и быстроживущих современных минчан и гостей столицы. 
								Уютность этого салона начинается с приветливости и заботы персонала и заканчивается необыкновенно комфортными креслами,
								прототипом которых является «папа-медведь»,	сделанными по индивидуальному заказу.
								В этих креслах каждый посетитель чувствует себя уникальной персоной.
							</p>
						</div>
						<div class="averagePhoto">
							<img src="/assets/index/img/safe.jpg" alt="">
							<div class="signature average">
								<p class="signature-head">Качество</p>
								<p class="signature-body">
									К каждому посетителю у нас индивидуальный подход. 
									Мы постоянно развиваемся, наши мастера повышают квалификацию,
									,используются новейшее оборудование и только качественная кометика. 
									Ведь для нас самое главное, чтобы абсолютно каждый посетитель "Сороки" , выходя из салона, уходил с 
									улыбкой на лице и был доволен результатом.
								</p>						
							</div>							
						</div>
						<div class="smallPhoto">
							<img src="/assets/index/img/about2.jpg" alt="">
							<div class="signature small">
								<p class="signature-head">Безопасность</p>
								<p class="signature-body"> 
									Инструменты стерилизуются после каждого клиента, поверхности, предметы и руки мастеров 
									обеззараживаются антисептиками.
								</p>					
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="aboutBlock manicureBottomBlock" id="viewport3">
			<div class="whiteClip">
				<a href="/" class="goMain"><span class="fa fa-arrow-left"></span><br><br>Вернуться <br>на главную</a>
				<div class="leftBlock">
					<ul class="menu" id="menu3">
						<li><a href="" id="menu3Viewport1">Популярные услуги</a></li>
						<li><a href="" id="menu3Viewport2">Наши задачи</a></li>
						<li><a href="" id="menu3Viewport3">Список всех услуг</a></li>
					</ul>
				</div>
			</div>
			<div class="container">
				<h2 class="title manicureViewport3Header">Цены на<br>маникюр</h2>
				<div class="content">
					<div class="procedures">
						<div class="procedure">
							<p class="procedureName procedureHeader">Название процедуры</p>
							<p class="timeHeader procedureHeader">Время</p>
							<p class="priceHeader procedureHeader">Цена</p>						
						</div>
						<div class="procedure" id="procedure1">
							<p class="procedureName">Классический маникюр</p>
							<p class="time">60 мин.</p>
							<p class="price">23 руб. 00 к.</p>
							<p class="booking" style="display: none">+375 29 699-29-30</p>
						</div>
						<div class="procedure" id="procedure2">
							<p class="procedureName">Европейский маникюр</p>
							<p class="time">60 мин.</p>
							<p class="price">23 руб. 00 к.</p>
							<p class="booking" style="display: none">+375 29 699-29-30</p>
						</div>	
						<div class="procedure" id="procedure3">
							<p class="procedureName">Городской маникюр</p>
							<p class="time">45 мин.</p>
							<p class="price">17 руб. 00 к.</p>
							<p class="booking" style="display: none">+375 29 699-29-30</p>
						</div>
						<div class="procedure" id="procedure4">
							<p class="procedureName">Классический педикюр</p>
							<p class="time">120 мин.</p>
							<p class="price">46 руб. 00 к.</p>
							<p class="booking" style="display: none">+375 29 699-29-30</p>
						</div>
						<div class="procedure" id="procedure5">
							<p class="procedureName">Комбинированный педикюр</p>
							<p class="time">120 мин.</p>
							<p class="price">58 руб. 00 к.</p>
							<p class="booking" style="display: none">+375 29 699-29-30</p>
						</div>						
						<div class="procedure" id="procedure6">
							<p class="procedureName">Аппаратный педикюр</p>
							<p class="time">120 мин.</p>
							<p class="price">58 руб. 00 к.</p>
							<p class="booking" style="display: none">+375 29 699-29-30</p>
						</div>
						<div class="procedure" id="procedure17">
							<p class="procedureName">Кислотный педикюр</p>
							<p class="time">120 мин.</p>
							<p class="price">52 руб. 00 к.</p>
							<p class="booking" style="display: none">+375 29 699-29-30</p>
						</div>						
						<div class="procedure" id="procedure7">
							<p class="procedureName">Покрытие ногтей на гель-лак Аэрография(амбрэ)</p>
							<p class="time">От 40 мин.</p>
							<p class="price">23 руб. 00 к.</p>
							<p class="booking" style="display: none">+375 29 699-29-30</p>
						</div>
						<div class="procedure" id="procedure8">
							<p class="procedureName">Покрытие ногтей гель-лаком однотонное </p>
							<p class="time">От 40 мин.</p>
							<p class="price">35 руб. 00 к.</p>
							<p class="booking" style="display: none">+375 29 699-29-30</p>
						</div>
						<div class="procedure" id="procedure9">
							<p class="procedureName">Покрытие ногтей гель-лаком френч</p>
							<p class="time">От 40 мин.</p>
							<p class="price">35 руб. 00 к.</p>
							<p class="booking" style="display: none">+375 29 699-29-30</p>
						</div>
						<div class="procedure" id="procedure10">
							<p class="procedureName">Парафин кистей рук</p>
							<p class="time">От 40 мин.</p>
							<p class="price">17 руб. 00 к.</p>
							<p class="booking" style="display: none">+375 29 699-29-30</p>
						</div>
						<div class="procedure" id="procedure11">
							<p class="procedureName">Парафин ног</p>
							<p class="time">От 40 мин.</p>
							<p class="price">23 руб. 00 к.</p>
							<p class="booking" style="display: none">+375 29 699-29-30</p>
						</div>
						<div class="procedure" id="procedure12">
							<p class="procedureName">Окраска бровей</p>
							<p class="time">От 40 мин.</p>
							<p class="price">12 руб. 00 к.</p>
							<p class="booking" style="display: none">+375 29 699-29-30</p>
						</div>
						<div class="procedure" id="procedure13">
							<p class="procedureName">Окраска бровей хной</p>
							<p class="time">От 40 мин.</p>
							<p class="price">17 руб. 00 к.</p>
							<p class="booking" style="display: none">+375 29 699-29-30</p>
						</div>
						<div class="procedure" id="procedure14">
							<p class="procedureName">Коррекция бровей</p>
							<p class="time">От 40 мин.</p>
							<p class="price">12 руб. 00 к.</p>
							<p class="booking" style="display: none">+375 29 699-29-30</p>
						</div>
						<div class="procedure" id="procedure15">
							<p class="procedureName">Окраска ресниц</p>
							<p class="time">От 40 мин.</p>
							<p class="price">12 руб. 00 к.</p>
							<p class="booking" style="display: none">+375 29 699-29-30</p>
						</div>
						<div class="procedure" id="procedure16">
							<p class="procedureName">Наращивание ногтей</p>
							<p class="time">От 40 мин.</p>
							<p class="price">63 руб. 00 к.</p>
							<p class="booking" style="display: none">+375 29 699-29-30</p>
						</div>
					</div>
				</div>
			</div>
			<div class="footer">
				<div class="soc_networks">
					<a target="_blank" href="https://www.instagram.com/salon_soroki/" class="fa fa-instagram"></a>
					<a target="_blank" href="https://www.facebook.com/salonsoroki/" class="fa fa-facebook"></a>
				</div>
				<p>г.Минск, ул.Революционная 7<br>+375 (29) 699-29-30</p>
				<p>&copy 2008-2017 ООО Дефиле<br>Все права защищены</p>
			</div>			
		</div>
	</body>
	<script src="/assets/index/js/jquery-3.2.1.min.js"></script>
	<script>
		window.onload = function () {
			$('html,body').css('opacity', '1');
			for(let j = 1; j <= 3; j++) {
				for(let i = 1; i <= $('#menu' + j).children().length; i++) {
					$('#menu' + j + 'Viewport' + i).on('click', (event) => {
						event.preventDefault();
						let destination = $('#viewport' + i).position().top;
						$('HTML, BODY').animate({ scrollTop: destination }, 1000);		
					});
				}
			}			
		};
		$('#openMenu').on('click', function (event) {
				event.preventDefault();
				$(".mobileMenu").css('left') == '0px' ? $(".mobileMenu").css('left','-100%') : $(".mobileMenu").css('left','0');
			});	
			$('#closeMenu').on('click', function (event) {
				event.preventDefault();
				$(".mobileMenu").css('left', '-100%');
			})		
	</script>
</html>