<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{

    public function send(Request $request){
        $title = 'Новое сообщение с soroki.by';
        $name = $request->input('name', '');
        $email = $request->input('email', '');
        $number = $request->input('number', '');
        $text = $request->input('text', '');

        Mail::send(
            'emails.order',
            [
                'title' => $title,
                'name' => $name,
                'email' => $email,
                'number' => $number,
                'text' => $text
            ],
            function ($message) use ($title)
            {

                $message->from('soroki.by@gmail.com', $title);

                $message->to('soroki.by@gmail.com')->subject($title);

            }
        );

        return redirect('/')->with('flash_message', 'Ваша заявка принята');

        // return response()->json(['message' => 'Request completed']);
    }
}
