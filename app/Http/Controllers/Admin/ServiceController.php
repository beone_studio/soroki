<?php

namespace App\Http\Controllers\Admin;

use App\Service;
use App\Category;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServiceController extends Controller {

	public function index() {
		return Service::
				with('category')
				->orderBy('category_id', 'ASC')
				->get();
	}

	public function show(Service $service) {
		return $service;
	}

	public function getCategories() {
		return Category::select('id', 'name')->get();
	}

	public function create(Request $request) {
		Service::create($request->all());

		return ['result' => 'success'];
	}

	public function update(Service $service, Request $request) {
		$service->update($request->all());

		return ['result' => 'success'];
	}

	public function delete(Service $service) {
		$service->delete();

		return ['result' => 'success'];
	}

	public function postFileWithServices(Request $request) {
		$content = File::get($request->input('services'));

		echo $content;
	}
}