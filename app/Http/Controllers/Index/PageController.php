<?php

namespace App\Http\Controllers\Index;

use App\Category;
use App\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
	public function index($page)
	{	
		if (view()->exists('index.services.' . $page)) { 
			 return view('index.services.' . $page); 
		} else {
			abort(404);
		}
	}
}