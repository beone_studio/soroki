<?php

namespace App\Http\Controllers\Index;

use App\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
    	$services = Service::orderBy('category_id')->get();
        return view('index.main', compact('services'));
    }
    public function test()
    {
        return view('index.test');
    }
}
