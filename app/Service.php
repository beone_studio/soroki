<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = [
    	'name',
    	'category_id',
    	'time',
    	'price'
    ];

    public function category() {
    	return $this->belongsTo(Category::class);
    }
}
