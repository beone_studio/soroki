<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
    Route::get('/', 'AuthController@login');
    Route::post('/', 'AuthController@doLogin');
    Route::get('/logout', 'AuthController@logout');

    Route::group(['middleware' => 'auth'], function () {
        Route::group(['prefix' => 'api'], function () {
            Route::group(['prefix' => 'services'], function () {
                Route::get('/', 'ServiceController@index');
                Route::post('/', 'ServiceController@create');
                Route::get('/{service}', 'ServiceController@show');
                Route::post('/{service}', 'ServiceController@update');
                Route::delete('/{service}', 'ServiceController@delete');
            });
            Route::get('/categories', 'ServiceController@getCategories');
            Route::post('/service_file', 'ServiceController@postFileWithServices');
        });

        Route::any('{all?}', function () {
            return view('admin.main');
        })->where(['all' => '.*']);
    });
});

Route::post('/send', 'EmailController@send');

/*Route::group(['namespace' => 'Index'], function () {
    Route::get('/', 'HomeController@test');
});*/

Route::group(['namespace' => 'Index'], function () {
    Route::get('/', 'HomeController@index');
    Route::get('/services', 'ServicesController@index');
    Route::get('/services/{page}', 'PageController@index');
    Route::get('/contacts', 'ContactsController@index');
    Route::get('/depilation', 'DepilationController@index');
    Route::get('/about', 'AboutController@index');
});
