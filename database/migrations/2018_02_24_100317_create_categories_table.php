<?php

use App\Category;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('categories');
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->timestamps();
        });

        Category::create([
            'name' => 'Маникюр',
            'slug' => 'manikyur'
        ]);
        Category::create([
            'name' => 'Педикюр',
            'slug' => 'pedikyur'
        ]);
        Category::create([
            'name' => 'Брови и ресницы',
            'slug' => 'brovi'
        ]); 
        Category::create([
            'name' => 'Наращивание ногтей',
            'slug' => 'narashhivanie'
        ]);  
        Category::create([
            'name' => 'Парафинотерапия',
            'slug' => 'parafin'
        ]);
        Category::create([
            'name' => 'Покрытие гель-лаком',
            'slug' => 'pokrytie'
        ]);                                     
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
